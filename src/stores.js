import { writable } from "svelte/store";

export const userbaseStore = writable(null)
export const userStore = writable(null)
export const promiseStore = writable(null)
export const navStore = writable(null)